<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::create([
            'name'=>'admin',
            'role_id'=>'1',
            'email'=>'admin@admin.com',
            'password'=> bcrypt('123123'),
        ]);
        \App\Models\Role::create([
            'name'=>'admin',
        ]);
        \App\Models\Role::create([
            'name'=>'user',
        ]);

    }
}
