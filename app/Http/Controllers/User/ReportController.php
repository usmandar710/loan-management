<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReportResource;
use App\Models\Transaction;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function list(Request $request)
    {
        $q = Transaction::query();
        if ($request->has('user_id') && !empty($request->user_id)){
            $q->where('user_id',$request->user_id);
        }
        $reports =  $q->with('user')->orderBy('id','desc')->paginate(10);

        return success_response('Payment list',new ReportResource($reports));
    }
}
