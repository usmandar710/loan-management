<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\PeopleResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()){
            $response = error_response('Invalid Data', $validator->errors()->first());
            return response()->json($response);
        }
        $findUser = User::where('email',$request->email)->first();
        if(!empty($findUser)){
            $response = error_response('Invalid Data', "Email Already Exist");
            return response()->json($response);
        }
        $user = User::create([
            'name'=> $request->name,
            'email'=>$request->email,
            'description'=>$request->description,
            'contact'=>$request->contact,
            'address'=>$request->address,
            'role_id'=>'2',
        ]);
        return success_response('User created successfully',$user);
    }

    public function show()
    {
        try {
            $users = User::where('role_id', '2')->latest()->paginate(10);

            return success_response('User list', new PeopleResource($users));
        }catch (\Exception $exception){
            return error_response('Request Failed',$exception->getMessage());
        }
    }

    public function getAllUsers()
    {
        try {
            $users = User::where('role_id', '2')->latest()->get();

            return success_response('User list', $users);
        }catch (\Exception $exception){
            return error_response('Request Failed',$exception->getMessage());
        }
    }
    public function edit($id)
    {
        $user = User::find($id);
        if ($user){
            return success_response('User detail',$user);
        }else{
            return error_response("Request Failed",'User Not Found');
        }
    }
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->fails()){
            $response = error_response('Invalid Data', $validator->errors()->first());
            return response()->json($response);
        }
        $user = User::find($id);
        if ($user){
            $user->name =$request->name;
            $user->email=$request->email;
            $user->description=$request->description;
            $user->contact=$request->contact;
            $user->address=$request->address;
            $user->save();
            return success_response('User updated successfully',$user);
        }else{
            return error_response("Request Failed",'User Not Found');
        }
    }
    public function delete($id)
    {
        $user = User::find($id);
        if ($user){
            $user->delete();
            return success_response('User deleted successfully');
        }else{
            return error_response("Request Failed",'User Not Found');
        }
    }
}
