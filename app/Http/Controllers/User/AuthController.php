<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\AuthUserDetailResource;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',
        ]);

        try{
            if($validator->fails()){
                $response = error_response('Invalid Data', $validator->errors()->first());
                return response()->json($response);
            }

            $username = $request->input('email'); //the input field has name='username' in form

            if(filter_var($username, FILTER_VALIDATE_EMAIL)) {
                $credentials = [
                    'email' => $username,
                    'password' => $request->input('password')
                ];
                if(!User::CheckUser($request->input('email'))) {
                    $response = error_response('Invalid Credentials','User Didn\'t Exists.', '425');
                    return response()->json($response);
                }

                $user = User::where('email' , $username)->first();
            } else {
                $credentials = [
                    'user_name' => $username,
                    'password' => $request->input('password')
                ];
                $user = User::where('user_name' , $username)->first();
            }


            if(!auth()->attempt($credentials)){
                $response = error_response('Invalid Credentials','Password Didn\'t Match With Our Records.');
                return response()->json($response);
            }


            $accessToken = $user->createToken('authToken')->accessToken;

            $response = [
                'statusCode' => '200',
                'status' => 'Success',
                'message' => 'Successfully logged In.',
                'data' => [
                    'token' => $accessToken,
                    'userData' => new AuthUserDetailResource($user),
                ]
            ];

            return response()->json($response);
        }
        catch (\Exception $e){
            $response = error_response('Login Fails', $e->getMessage());
            return response()->json($response);
        }
    }

}
