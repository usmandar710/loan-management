<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\ReportResource;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $transactions = Transaction::with('user')->orderBy('id','desc')->paginate(5);
        $personCard = User::where('role_id',2)->count();
        $totalDebit = Transaction::where('type','borrowed')->get()->groupBy('currency');

        $totalPkrDebit = isset($totalDebit['pkr']) ? $totalDebit['pkr']->sum('amount') : 0;
        $totalRiyalDebit = isset($totalDebit['riyal']) ? $totalDebit['riyal']->sum('amount') : 0;
        $totalReceived =  Transaction::where('type','received')->get()->groupBy('currency');
        $totalPkrReceived = isset($totalReceived['pkr']) ? $totalReceived['pkr']->sum('amount') : 0;
        $totalRiyalReceived = isset($totalReceived['riyal']) ? $totalReceived['riyal']->sum('amount') : 0;

        $debitCard = [
            'pkr'=> $totalPkrDebit,
            'riyal'=> $totalRiyalDebit,
            ];
        $receivedCard = [
            'pkr' => $totalPkrReceived,
            'riyal' => $totalRiyalReceived,
        ];
        $pendingCard = [
            'pkr' => $totalPkrDebit-$totalPkrReceived,
            'riyal' => $totalRiyalDebit-$totalRiyalReceived,
        ];
        return success_response('Dashboard data',[
           'transactions' => new ReportResource($transactions),
            'person_card' => $personCard,
            'debit_card' => $debitCard,
            'received_card' => $receivedCard,
            'pending_card' => $pendingCard,
        ]);

    }
}
