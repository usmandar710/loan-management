<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\PeopleResource;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'type' => 'required',
            'amount'=>'required',
            'currency'=>'required',
            'time'=>'required',
            'date'=>'required',
        ]);

        if($validator->fails()){
            $response = error_response('Invalid Data', $validator->errors()->first());
            return response()->json($response);
        }
        $user = User::findOrFail($request->user_id);

        $userBalance = $user->balance;

        if ($request->type === "borrowed"){
            $userBalance += $request->amount;
        }else{
            $userBalance -= $request->amount;
        }
        $user->update(['balance' => $userBalance]);

        $transaction = Transaction::create([
            'type'=>$request->type,
            'amount'=>$request->amount,
            'currency'=>$request->currency,
            'time'=>$request->time,
            'date'=>$request->date,
            'description'=>$request->description,
            'user_id'=>$request->user_id,
            'balance'=>$userBalance,
        ]);
        return success_response('Transaction saved successfully',$transaction);
    }
    public function show()
    {
        $transactions = Transaction::orderBy('id','desc')->paginate(10);
        return success_response('Transaction list',$transactions);
    }

    public function userTransaction($id)
    {
        $user = User::findOrFail($id);

        return success_response('User transactions',[
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'contact' => $user->contact,
                'current_debit' => $user->balance,
                'total_borrowed' => $user->totalBorrowed()->sum('amount'),
                'total_received' => $user->totalReceived()->sum('amount'),
                ],
            'transactions' => $user->transactions()->paginate(10),
            ]);
    }
}
