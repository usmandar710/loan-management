<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PeopleResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $peoples =[];
       foreach ($this->collection as $collect){
           $people = [
               'id' => $collect->id,
               'name' => $collect->name,
               'contact' => $collect->contact,
               'current_debit' => $collect->balance,
               'total_borrowed' => $collect->totalBorrowed()->sum('amount'),
               'total_received' => $collect->totalReceived()->sum('amount'),
           ];

           array_push($peoples,$people);
       }

       $pagination = [
           'total' => $this->total(),
           'count' => $this->count(),
           'per_page' => $this->perPage(),
           'current_page' => $this->currentPage(),
           'next_page_url' => $this->nextPageUrl(),
           'previous_page_url' => $this->previousPageUrl(),
           'total_pages' => $this->lastPage()
       ];

       return [
         'peoples'  => $peoples,
         'pagination'  => $pagination,
       ];
    }
}
