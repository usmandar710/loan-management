<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportResource extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $reports =[];
        foreach ($this->collection as $collect){
            $report = [
                'person_id' => $collect->user ? $collect->user->id : 'N/A',
                'person_name' => $collect->user ? $collect->user->name : 'N/A',
                'amount' => $collect->amount,
                'type' => $collect->type,
                'date' => $collect->date,
                'time' => $collect->time,
                'currency' => $collect->currency,
            ];

            array_push($reports,$report);
        }

        $pagination = [
            'total' => $this->total(),
            'count' => $this->count(),
            'per_page' => $this->perPage(),
            'current_page' => $this->currentPage(),
            'next_page_url' => $this->nextPageUrl(),
            'previous_page_url' => $this->previousPageUrl(),
            'total_pages' => $this->lastPage()
        ];

        return [
            'reports'  => $reports,
            'pagination'  => $pagination,
        ];
    }
}
