<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Role;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'role_id',
        'password',
        'contact',
        'address',
        'description',
        'balance'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function scopeCheckUser($query, $email){
        $user = $query->where('email' , $email)->first();

        if($user){
            return true;
        }
        return false;
    }
    public function role(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function isAdmin()
    {
        return $this->admin;
    }
    public function transactions()
    {
        return $this->hasMany(Transaction::class,'user_id','id');
    }
    public function totalBorrowed()
    {
        return $this->hasMany(Transaction::class,'user_id','id')
            ->where('type','borrowed');
    }

    public function totalReceived()
    {
        return $this->hasMany(Transaction::class,'user_id','id')
            ->where('type','received');
    }

}
