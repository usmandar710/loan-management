<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => "v1",'namespace' => "User"],function(){
    Route::post('/login','AuthController@login');

    Route::middleware(['auth:api'])->group(function () {
        Route::group(['prefix' => 'user'], function () {
            Route::post('/create','UserController@create');
            Route::get('/list','UserController@show');
            Route::get('/get/all/users','UserController@getAllUsers');
            Route::get('/edit/{id}','UserController@edit');
            Route::post('/update/{id}','UserController@update');
            Route::post('/delete/{id}','UserController@delete');
        });
        Route::group(['prefix' => 'transaction'], function () {
            Route::post('/create','TransactionController@create');
            Route::get('/list','TransactionController@show');
            Route::get('/user/{id}','TransactionController@userTransaction');
        });
        Route::group(['prefix' => 'report'], function () {
            Route::get('/list','ReportController@list');
        });
        Route::get('/dashboard','DashboardController@dashboard');
    });
});

